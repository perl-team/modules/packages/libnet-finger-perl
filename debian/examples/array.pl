#!/usr/bin/perl
# 
# Simple example. Finger a certain Host and put its response
# into a list, then Dump this list with Data::Dumper.
#
# See: http://search.cpan.org/~fimm/Net-Finger-1.06/Finger.pm
# for more examples.
#
use Net::Finger;
use Data::Dumper;
@lines = finger('user@192.168.1.1', 1);
print Dumper(@lines);

